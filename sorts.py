import numpy as np
from random import randint

def quicksort(arr):
  arr = list(arr)
  N = len(arr)
  if N <= 1:
    return arr

  pivot = arr[randint(0, N-1)]
  left, equal, right = [], [], []

  for i in arr:
    if i < pivot:
      left.append(i)
    elif i > pivot:
      right.append(i)
    else:
      equal.append(i)

  return quicksort(left) + equal + quicksort(right)

def partition(a, low, high):
  i = low-1
  pivot = a[high]

  for j in range(low, high):
    if a[j] <= pivot:
      i += 1
      a[i], a[j] = a[j], a[i]

  a[i+1], a[high] = a[high], a[i+1]
  return a, i+1

def quick_sort_inplace(a, low=0, high=None):
  if high == None:
    high = len(a)-1
  if low < high:
    a, p_index = partition(a, low, high)
    # print(a[low: p_index], ':', a[p_index], ':', a[p_index+1: high+1])
    quick_sort_inplace(a, low, p_index-1)
    quick_sort_inplace(a, p_index+1, high)
  return a





def insertion_sort(list):
  print(list)
  for i in range(1, len(list)):

    key = list[i]

    j = i-1
    while j >=0 and key < list[j]:
      list[j+1], list[j] = list[j], list[j+1]
      j-=1
  return list

def insertion_sort_recursion(list, n):
  if n <= 1:
    return
  insertion_sort_recursion(list, n-1)
  last = n-1
  for i in range(0, last):
    if list[i] > list[last]:
        list[i], list[last] = list[last], list[i]
  print(list)

def insertion(list):
  # print(list)
  for i in range(1, len(list)):
    for j in range(0, i):
      if list[j] > list[i]:
        list[j], list[i] = list[i], list[j]
  return list
