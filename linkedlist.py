import numpy as np

class Node:
  def __init__(self, value, next=None):
    self.value = value
    self.next = None

  def __str__(self):
    cur_node = self
    res = []
    while cur_node:
      res.append(cur_node.value)
      cur_node = cur_node.next
    return str(res)

class LinkedList:
  def __init__(self, head=None):
    self.head = head

  def reverse(self):
    pre = None
    cur = self.head

    while cur is not None:
      old_next = cur.next
      cur.next = pre
      pre = cur
      cur = old_next
    self.head = pre

  def remove_kth_element(self, value):
    cur_node, pre_node = self.head, self.head

    if value == 1:
      self.head = self.head.next
      cur_node = None
      return

    for i in range(value-1):
      if i == value-2:
        pre_node = cur_node
      cur_node = cur_node.next

    pre_node.next = cur_node.next
    cur_node = None

  def insert_start(self, value):
    new_node = Node(value)
    if self.head:
      new_node.next = self.head
      self.head = new_node
    else:
      self.head = new_node

  def insert_end(self, value):
    new_node = Node(value)
    cur_node = self.head
    if not self.head:
      self.head = new_node
    else:
      while cur_node.next is not None:
        cur_node = cur_node.next
      cur_node.next = new_node
        # new_node.next = None


  def print(self):
    cur_node = self.head
    while cur_node:
      print(cur_node.value, end=':')
      cur_node = cur_node.next


































# class Node(object):
#   def __init__(self, data):
#     self.data = data
#     self.nextNode = None
#
#   def removeNode(self, data, previousNode):
#     if self.data == data:
#       previousNode.nextNode = self.nextNode
#       del self.data
#       del self.nextNode
#     else:
#       if self.nextNode is not None:
#         self.nextNode.removeNode(data, self)
#
# class linkedlist(object):
#   def __init__(self):
#     self.head = None
#     self.count = 0
#
#   #O(1)
#   def insertStart(self, data):
#     self.count +=1
#
#     newNode = Node(data)
#     if not self.head:
#       self.head = newNode
#     else:
#       newNode.nextNode = self.head
#       self.head = newNode
#
#   def size(self):
#     return self.count
#
#   # O(N)
#   def insertEnd(self, data):
#     if self.head is None:
#       self.insertStart(data)
#       return
#     self.count+=1
#
#     newNode = Node(data)
#     actualNode = self.head
#
#     while actualNode.nextNode is not None:
#       actualNode = actualNode.nextNode
#     actualNode.nextNode = newNode
#
#   def remove(self, data):
#     self.count -= 1
#     if self.head:
#         if data == self.head.data:
#           self.head = self.head.nextNode
#         else:
#           self.head.removeNode(data, self.head)
#
#   def traverseList(self):
#     # actualNode = self.head
#     while self.head is not None:
#       print("%d" % self.head.data)
#       self.head = self.head.nextNode