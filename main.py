# Import libraries
import numpy as np
import timeit
# Import classes & functions
import functions, sorts, linkedlist
import math
from random import randint as RANDOM_INTEGER

if __name__ == '__main__':
  arr = np.array([10, 20, 19, 40, 50, 60])
  arr1 = np.array([2, 2, 4, 4, 4, 4])
  arr2 = np.array([5, 1, 6, 2,4, 3])
  arr3 = np.random.randint(2, 100, size=5)
  arr4 = [0, 5, 0, 3]
  people = [3, 6, 3, 4, 1]
  string = "ABC"
  C = "c"
  list = [[1,2], [3,4]]
  grid = [[1, 2, 3, 4, 5],
          [6, 7, 8, 9, 10],
          [11, 12, 13, 14, 15],
          [16, 17, 18, 19, 20]]
  courses = {
    'CSC400': ['CSC100', 'CSC200', 'CSC300'],
    'CSC300': ['CSC100', 'CSC200'],
    'CSC200': ['CSC100'],
    'CSC100': []
  }
  # functions.people_see_murder(people)
  a = linkedlist.LinkedList()
  for _ in range(3):
    a.insert_start(RANDOM_INTEGER(1, 10))
  a.print()
  print('\n')
  a.reverse()
  # a.remove_kth_element(3)
  a.print()
  # start = timeit.default_timer()
  # stop = timeit.default_timer()
  # time1 = stop - start
  # print('Time1: ', time1)

  # student = {"name": "Hieu",
  #            "marks": [6,7],
  #            "exams":{
  #             "final": 8,
  #             "midterm": 9}}
  #
  # # print(student['exams']['final'])
  # # all_mark = len(student['marks'])
  # # marks = np.sum(student['marks'])
  # # print(marks/all_mark)
  # functions.menu()