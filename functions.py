import numpy as np
import sorts, os, sys

# There are n people lined up, and each have a height represented as an integer. A murder has happened right in front of them, and only people who are taller than everyone in front of them are able to see what has happened. How many witnesses are there?
#  Example:
# Input: [3, 6, 3, 4, 1]
# Output: 3
# Explanation: Only [6, 4, 1] were able to see in front of them.
def people_see_murder(a):
  res = []
  check = 0
  for i in range(len(a)):
    for j in range(i+1, len(a)):
      if a[j] > a[i]:
        check = 1
        break
    if check == 1:
      check = 0
    else:
      res.append(a[i])
  print(res)

def check_prerequisites(courses, course):
  courses = dict(courses)
  res = []
  for  value in courses[course]:
    if value not in  courses.keys():
      print('No Link')
      return
    else:
     res.append(value)
  print(res)

     # print(courses[value], end=':')

def move_zeros_to_end(a):
  count=0
  a=list(a)

  for i in range(len(a)):
    if a[i] == 0:
      a.append(a.pop(i))
  return a


def find_k_largest_element(arr, k):
  arr = list(arr)
  arr = sorts.quicksort(arr)
  print(arr)
  return arr[-k]


def print_spiral_traversal_of_grid(grid):
  grid = np.asarray(grid)
  [N, D] = grid.shape
  print(grid, '\n', '----------')
  row = 0
  col = 0

  while row < N and col < D:
    # left to right
    for d in range(col, D):
      print(grid[row, d], end=' ')
    row +=1

    # up to down
    for i in range(row, N):
      print(grid[i, D-1], end=' ')
    D-=1

    # right to left
    for d in reversed(range(col, D)):
      print(grid[N-1, d], end=' ')
    N-=1

    # down to up
    for i in reversed(range(row, N)):
      print(grid[i, col], end=' ')
    col+=1

# Reverse array
def reverse_array(arr):
  for i in range (0, int(len(arr)/2)):
    other = len(arr) - i - 1
    arr[other], arr[i] = arr[i], arr[other]
    # print(arr)
  return arr

# Get all permutations of a string
def to_string(List):
  return ''.join(List)
def permutation(a, l, r):
  if l == r:
    print(to_string(a))
  else:
    for i in range(l, r+1):
      a[l], a[i] = a[i], a[l]
      permutation(a, l + 1, r)
      a[l], a[i] = a[i], a[l]

# Get all permutations of a string using recursion
def permu_recursion(string):
  string = list(string)
  if len(string) == 1:
    return string
  else:
    return add_char_to_string(list(permu_recursion(string[:-1])), list(string[len(string)-1]))
# Add a character into all possible positions in a string
def add_char_to_string(string, char):
  res = []
  for i in range(0, len(string)):
    temp = list(string[i])
    for j in range(0, len(temp)+1):
      res.append(temp[:j] + char + temp[j:])
    # print(res)
  return res

# Get n-th Fibonacci number
def fibonacci(n):
  if n < 0:
    return 0
  elif n == 1:
    return 1
  return fibonacci(n-1) + fibonacci(n-2)

# Print n Fibonacci numbers
def get_n_fib(n):
  memo = np.zeros(n+1)
  for i in range (0, n+1):
    print(fib(i, memo))
# Get n-th Fibonacci number with memorization
def fib(n, memo):
  if n <= 0:
    memo[n] = 0
    return 0
  elif n == 1:
    memo[n] = 1
    return 1
  else:
    memo[n] = memo[n-1] + memo[n-2]
    # print(memo)
    return memo[n]

# Print Power of 2 from 1 to N
def get_power_2_to_N(n):
  if n < 1:
    return 0
  elif n == 1:
    print(1)
    return 1
  else:
    prev = get_power_2_to_N(int(n/2))
    curr = prev * 2
    print(curr)
    return curr

# Print a % b (remainder of division)
def mod(a, b):
  if b <= 0:
    return 0
  else:
    div = int(a / b)
    return a - div * b

# Print all pairs with sum k in list
def pairs_with_sum_k(list, k):
  if all(list[i] < list[i+1] for i in range(len(list)-1)) is False:
    list = sorts.insertion(list)
  for i in range(0, len(list)):
    for j in range(i+1, len(list)):
      if list[i] + list[j] == k:
        print(list[i], list[j])

def pairs_with_sum_k_2(list, k):
  dic = {}
  for i in range(len(list)):
    dic[list[i]] = k - list[i]
  print(dic)
  for key in dic.keys():
    for val in dic.values():
      if key == val:
        print(key, dic.get(key))
  # still have duplicates
      # temp = k - list[i]
      # temp_list.append(temp)
      # for j in range(len(temp_list)):
      #   if list[i] == temp_list[j]:
      #     print(list[i], temp_list[j])

def move_1_step(list, size):
  for i in range(0, len(list)-size+1):
    temp = np.zeros(size)
    count = 0
    while count < size:
      temp[count] = list[i+count]
      count +=1
    print(temp)

def check_palindrome(s):
  string = list(s)
  if len(s) % 2 == 0:
    if reverse_array(string[0: int(len(s)/2)]) == string[int(len(s)/2)::]:
      return True
    else:
      return False
  else:
    if reverse_array(string[0: int(len(s)/2)]) == string[int(len(s)/2)+1::]:
      return True
    else:
      return False

def find_longest_palidrome(s):
  s = list(s)
  longest = []
  longest_list = []
  for i in range(0, len(s)):
    for j in range(i+2, len(s)+1):
      if check_palindrome(s[i:j]):
        if len(s[i:j]) > len(longest):
          longest_list = []
          longest = s[i:j]
          longest_list = s[i:j]
        elif len(s[i:j]) == len(longest):
          longest_list = np.vstack((longest_list, s[i: j]))
  print(longest_list)

def validate_balanced_parenthesis(s):
  s = list(s)
  count_round = 0
  count_sqr = 0
  count_point = 0
  for i in range(len(s)):
    if s[i] == '(':
      count_round +=1
    elif s[i] == '[':
      count_sqr +=1
    elif s[i] == '{':
      count_point +=1
    elif s[i] == ')':
      count_round -=1
    elif s[i] == ']':
      count_sqr -=1
    elif s[i] == '}':
      count_point -=1
  if count_round == count_sqr == count_point == 0:
    print('Good')
  else:
    print('NO')

def find_first_last_element_in_array(arr, k):
  first = -1
  last = -1
  for i in range(len(arr)):
    if arr[i] == k:
      if first == last == -1:
        first = i
        last = i
      else:
        last = i
  print(first, last)

def sort_list_of_3_unique_numbers(arr):
  arr = list(arr)
  min = np.min(arr)
  max = np.max(arr)
  res = []
  for i in range(len(arr)):
    if arr[i] == min:
      res.insert(0, arr[i])
    elif arr[i] == max:
      res.insert(len(res), arr[i])
    else:
      res.insert(int(len(arr)/2), arr[i])
  print(res)

def find_unique_number(str):
  str = list(str)
  for i in range(len(str)):
    count = 0
    if i == 0:
      for j in range(i+1, len(str)):
        if str[i] == str[j]:
          count += 1
      if count == 0:
        print(str[i])
    else:
      j = 0
      while j < len(str) and j != i:
        if str[i] == str[j]:
          count +=1
        j+=1
      if count == 0:
        print(str[i])

def check_almost_sorted_list(s):
  s = list(s)
  count = 0
  for i in range(len(s)-1):
    if s[i] > s[i+1]:
      count+=1
  if count > 1:
    print('NO')
  else:
    print('GOOD')
  print(s)

def count_pairs(s, k):
  s = list(s)
  count = 0
  i = 0
  while i < len(s)-1:
    if s[i] == s[i+1] == k:
      count +=1
    i+=2
  print(count)

def create_student(dic):
  # Ask user for student's name
  student_name = input('Enter name: ')

  # Create dictionary in format {'name':<name>, 'marks': []}
  dic['name'] = student_name
  return dic

def add_mark(dic, mark):
  dic['marks'].append(mark)

def average_mark(dic):
  number = len(dic['marks'])
  if number == 0:
    return 0

  total = np.sum(dic['marks'])
  return total / number

def print_list(list):
  for id, item in enumerate(list):
    print('ID: ', id)
    print_student_details(item)

def print_student_details(student):
  print("{}, average mark: {}.".format(student['name'], average_mark(student)))

def menu():
  dic = {'name': None, 'marks': []}
  list_dic = []
  choice = input('a - add new student, m - add mark, am - average, p - print, q - quit: ')
  while choice != 'q':
    if choice == 'a':
      list_dic.append(create_student(dic))
    elif choice == 'p':
      print_list(list_dic)
    elif choice == 'm':
      id = input('enter id: ')
      mark = input('enter mark: ')

      add_mark(list_dic[int(id)], mark)
    os.system('clear')
    choice = input('a - add new student, m - add mark, am - average, p - print, q - quit: ')

  # return dictionary

def max_product_3_elements(arr):
  arr = list(arr)
  max = -sys.float_info.max

  for i, ai in enumerate(arr):
    for j, aj in enumerate(arr):
      for k, ak in enumerate(arr):
        if i != j != k:
          if ai*aj*ak > max:
            max = ai*aj*ak
    return max














