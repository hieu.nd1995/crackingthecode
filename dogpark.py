import numpy as np

class Dog:
  def __init__(self, name, age):
    self.name = name
    self.age = age


  def speak(self, content):
    pass



class Bulldog(Dog):
  atype = 'bulldog'

  def run(self, speed):
    print('{} is running {}'.format(Bulldog.name, speed))

  def print_info(self):
    return '{} is a {} years old {}'.format(self.name, self.age, self.atype)

a = Bulldog('hieu', 6)
print(a.print_info())